window.addEventListener('load', function () {
    let updateSvg = (svg) => {
        if (svg.contentDocument) {
            svg.parentElement.replaceChild(svg.contentDocument.documentElement.cloneNode(true), svg);
        }
        return svg.contentDocument;
    };
    Reveal.getSlides().forEach(function (slide) {
        slide.querySelectorAll('object').forEach(function (svg) {
            if (!updateSvg(svg)) {
                svg.addEventListener('load', function(){
                    updateSvg(svg);
                });
            }
        });
    });
});